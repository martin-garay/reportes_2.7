<?php
class reportes_ei_cuadro extends toba_ei_cuadro
{

	function get_columnas_dinamico($datos){
		$columnas = array();
		if(count($datos)>0){
			foreach ($datos[0] as $key => $col) {
				$columnas[] = array('clave'=>$key,'titulo'=>str_replace('_',' ',ucfirst($key))) ;
			}			
		}
		return $columnas;
	}
	function armar_cuadro_dinamico($datos, $borrar_columnas=true, $columnas=null){
		if($borrar_columnas)
			$columnas_eliminar = $this->get_columnas();
			if(count($columnas_eliminar)>0){
				$this->eliminar_columnas2($columnas_eliminar);
			}
		
		if(!isset($columnas)){
			$columnas = $this->get_columnas_dinamico($datos);
		}
		
		$this->agregar_columnas($columnas);
		$this->set_datos($datos);
	}
	function eliminar_columnas2($columnas)
	{
		foreach($columnas as $clave) {
			$id = $this->_info_cuadro_columna_indices[$clave['clave']];
			array_splice($this->_info_cuadro_columna, $id, 1);
			$this->procesar_definicion_columnas();		//Se re ejecuta por eliminación para actualizar $this->_info_cuadro_columna_indices	
		}
	}
	function set_cortes_colapsables(){
		$this->_info_cuadro['cc_modo'] = apex_cuadro_cc_tabular;
		//$this->_cortes_modo = apex_cuadro_cc_tabular;
		$this->_info_cuadro['cc_modo_anidado_colap'] = true;
	}
	function set_modo_cc_tabular(){
		$this->_info_cuadro['cc_modo'] = apex_cuadro_cc_tabular;
	}

	function set_mostrar_total_registros($mostrar=true){
		if($mostrar)
			$this->_info_cuadro['mostrar_total_registros'] = '1';
		else
			$this->_info_cuadro['mostrar_total_registros'] = '0';
	}

	protected function cargar_lista_eventos()
	{		
		/*Ordenamiento cuador: tengo que hacerlo con memoria antes de que se cargue el ci por que se define en la inicializacion 
		del componente y no tengo control ahi*/
		if(toba::memoria()->existe_dato('reporte_ordenable'))
			$this->_info_cuadro["ordenar"] = toba::memoria()->get_dato('reporte_ordenable');
		parent::cargar_lista_eventos();
	}
}
?>