<?php
class reportes_datos_tabla extends toba_datos_tabla
{
	function cargar($clave=array()){

        try {
            parent::cargar($clave);
        } catch (Exception $e) {
            throw new toba_error_usuario("Error Interno");
        }
    }
    
    function sincronizar($usar_cursores = false) {
        try{
            parent::sincronizar($usar_cursores);
        } catch (toba_error_db $e) {
                $mensaje= "Se produjo un error al guardar los datos. Consulte con el administrador del sistema";
                toba::notificacion()->error($mensaje);
                toba::logger()->error(dao_generico::get_mensaje_error($e));
        }
    }
}
?>