<?php
class ci_listado_reportes_publicos extends reportes_ci
{
	//-----------------------------------------------------------------------------------
	//---- cuadro -----------------------------------------------------------------------
	//-----------------------------------------------------------------------------------

	function conf__cuadro(reportes_ei_cuadro $cuadro)
	{		
        return toba::consulta_php('parametrizacion')->get_reportes_publicos("activo");		
	}

	function evt__cuadro__seleccion($seleccion)
	{
		$this->dep('datos')->limpiar_memoria();
		$this->dep('datos')->cargar($seleccion);
		toba::memoria()->set_dato('reporte_ordenable',$this->dep('datos')->es_ordenable()); //lo cargo antes de los eventos		
		$this->set_pantalla('pant_edicion');
	}

	function evt__cancelar(){
		$this->set_pantalla('pant_inicial');
	}

}

?>