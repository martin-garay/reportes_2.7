<?php
class ci_reporte_generico extends reportes_ci
{
	protected $s__filtro;
	protected $s__form_cortes;
	protected $s__es_inicial = true;
	protected $s__form_ml_columnas;

	function relacion(){
		return $this->dep('relacion');
	}
	function tabla($nombre){
		return $this->relacion()->tabla($nombre);
	}

	function ini__operacion(){		
		if(toba::memoria()->existe_dato('datos_reporte'))
			toba::memoria()->eliminar_dato('datos_reporte');
	}	
	
	function ini()
	{
		$columnas = $this->tabla('reporte_filtro')->get_filas();

		$this->dep('filtro')->crear_columnas_nuevas($columnas);
	}

	function conf__pant_inicial(toba_ei_pantalla $pantalla)
	{
		//formulario de cortes
		if(!$this->tabla('reporte')->get_columna('modifica_cortes'))
			$pantalla->eliminar_dep('form_cortes');
		else
			$this->dep('form_cortes')->colapsar();

		//formulario de columnas
		if(!$this->tabla('reporte')->get_columna('modifica_columnas'))
			$pantalla->eliminar_dep('form_ml_columnas');
		else
			$this->dep('form_ml_columnas')->colapsar();
	}

	function post_configurar(){
		if($this->existe_evento('ver_sql')){
			if(!toba::memoria()->existe_dato('datos_reporte'))
				$this->evento('ver_sql')->anular();
		}

	}	

	//-----------------------------------------------------------------------------------
	//---- form -------------------------------------------------------------------------
	//-----------------------------------------------------------------------------------

	function conf__form_cortes(reportes_ei_formulario $form)
	{
		
		//marco como tildados todos los cortes ya que al inicio se muestran todos
		if($this->s__es_inicial){
			$this->s__es_inicial = false;
			
			if($this->tabla('reporte_cortes')->get_cantidad_filas()>0){
				$cortes = $this->tabla('reporte_cortes')->get_filas();
				$this->s__form_cortes['cortes'] = implode(',', array_column($cortes, 'identificador')); 	
			}
		}

		//si selecciono algun corte se los paso al form
		if(isset($this->s__form_cortes['cortes'])){			
			$form->set_datos($this->s__form_cortes);
		}
	}

	function evt__form_cortes__modificacion($datos)
	{
		$this->s__form_cortes = $datos;
	}

	//-----------------------------------------------------------------------------------
	//---- form_ml_columnas -------------------------------------------------------------
	//-----------------------------------------------------------------------------------

	function conf__form_ml_columnas(reportes_ei_formulario_ml $form_ml)
	{
		if(!isset($this->s__form_ml_columnas))
			$this->s__form_ml_columnas  = $this->ordenar( $this->tabla('reporte_columnas')->get_filas() );
		$form_ml->set_datos($this->s__form_ml_columnas);
	}

	function evt__form_ml_columnas__modificacion($datos)
	{
		$this->s__form_ml_columnas = $datos;
	}
	


	//---- filtro -----------------------------------------------------------------------
	//-----------------------------------------------------------------------------------

	function conf__filtro(reportes_ei_filtro $filtro)
	{
		$filtro->set_datos($this->s__filtro);
	}

	function evt__filtro__filtrar($datos)
	{
		$this->s__filtro = $datos;
	}

	function evt__filtro__cancelar()
	{
		unset($this->s__filtro);
	}

	function get_sql_where(){
		$clausulas = $this->dep('filtro')->get_sql_clausulas();	
		$filtros = $this->tabla('reporte_filtro')->get_filas();
		
		//Saco las columnas que fueron definidas como parametro
		foreach ($filtros as $key => $filtro) {
			if($filtro['parametro'])
				unset($clausulas[$filtro['nombre']]);
		}		
		
		return $this->dep('filtro')->get_sql_where('and', $clausulas);		
	}

	function get_parametros_filtro(){
		$clausulas = $this->dep('filtro')->get_sql_clausulas();			
		$filtros = $this->tabla('reporte_filtro')->get_filas();
		$datos_filtro = $this->dep('filtro')->get_datos();		
		
		//Saco las columnas que fueron definidas como parametro
		foreach ($filtros as $key => $filtro) {
			if($filtro['parametro']){

				//si se debe pasar el valor reemplazo la clausula por el valor del filtro
				if( isset($clausulas[$filtro['nombre']]) ){
					if($filtro['tipo_parametro']=='valor')
						$clausulas[$filtro['nombre']] = $datos_filtro[$filtro['nombre']]['valor'];
				}else{					
					//si el usuario no envia el filtro seteo el valor por defecto (si existe)
					if(isset($filtro['estado_defecto'])){
						$clausulas[$filtro['nombre']] = $filtro['estado_defecto'];
					}else{
						
						// if($filtro['tipo_parametro']=='expresion') { $clausulas[$filtro['nombre']] = " 1=1 "}
						// si no existe el valor por defecto elimino para que se detecte que no se reemplazo la variable y no se ejecuta la consulta
						unset($clausulas[$filtro['nombre']]);
					}
				}
								
			}else{
				unset($clausulas[$filtro['nombre']]);
			}
		}		
		$parametros = array();
		//el formato de los parametros a reemplazar es array('nombre'=> , 'valor' => )
		foreach ($clausulas as $key => $value) {
			$parametros[] = array('nombre'=>$key, 'valor'=>$value);
		}
		return $parametros;
	}
	
	function conf__cuadro(reportes_ei_cuadro $cuadro)
	{
		//si no se filtro y hay variables obligatorias en el filtro no armo el cuadro
		if(!isset($this->s__filtro) && $this->existe_filtro_obligatorio()) {
			$cuadro->set_eof_mensaje("Por favor cargue los filtros obligatorios");
		} else{

			$cortes = $this->tabla('reporte_cortes')->get_filas();
			$cortes = $this->ordenar($cortes);
			if(count($cortes)>0){
				$cuadro->set_modo_cc_tabular();	//modo del cc
				$colapsable = $this->tabla('reporte')->get_columna('colapsar_niveles');
				if($colapsable)				
					$cuadro->set_cortes_colapsables();
				foreach ($cortes as $key => $corte) {

					if(isset($this->s__form_cortes)){
						//excluyo los cortes que no selecciono
						if( in_array($corte['identificador'], explode(',',$this->s__form_cortes['cortes']) ))
							$cuadro->agregar_corte_control($corte);
					}else{
						$cuadro->agregar_corte_control($corte);	
					}
				}
			}									
			$fuente_datos = $this->tabla('reporte')->get_columna('fuente');
			//$where = (isset($this->s__filtro)) ? 'WHERE '.$this->dep('filtro')->get_sql_where() : '';
			$where = (isset($this->s__filtro)) ? 'WHERE '.$this->get_sql_where() : '';
			$sql = "SELECT * FROM ( \n\n" . $this->tabla('reporte')->get_columna('query') . "\n\n) as subconsulta $where";

			$sql = toba::consulta_php('parametrizacion')->reemplazar_variables($sql, $this->get_parametros_filtro());		

			if(!toba::consulta_php('parametrizacion')->hay_variables_sin_reemplazar($sql)){
				try {
					$datos = toba::db($fuente_datos)->consultar($sql);

					$dato_memoria = array(	
						'sql' => $sql,
						'id' => $this->tabla('reporte')->get_columna('id')
					);
					toba::memoria()->set_dato('datos_reporte',$dato_memoria);
				} catch (toba_error_db $e) {
					throw new toba_error_usuario("Error al generar el reporte");			
				}			
			}else{
				$datos = array();
				$cuadro->set_eof_mensaje("Por favor filtre para buscar datos");
			}
			if($this->tabla('reporte')->get_columna('usar_ordenamiento_con_cortes'))
				$cuadro->set_usar_ordenamiento_con_cortes();

			if($this->tabla('reporte')->get_columna('mostrar_total_registros'))
				$cuadro->set_mostrar_total_registros();

			//si se definio en el abm las columnas a mostrar
			//$columnas_a_mostrar = ($this->tabla('reporte_columnas')->get_cantidad_filas()>0) ? $this->tabla('reporte_columnas')->get_filas() : null;			
			$columnas_a_mostrar = $this->get_columnas_a_mostrar();		
			$cuadro->armar_cuadro_dinamico($datos,true,$columnas_a_mostrar);
						
			$cuadro->set_titulo( $this->tabla('reporte')->get_columna('descripcion') );


		}
		
	}
	function existe_filtro_obligatorio(){
		$filtros = $this->tabla('reporte_filtro')->get_filas();

		$obligatorio = false;
		foreach ($filtros as $key => $filtro) {
			if($filtro['obligatorio'])
				$obligatorio = true;
		}
		return $obligatorio;
	}

	function get_columnas_a_mostrar(){				
		$columnas_a_mostrar = ($this->tabla('reporte_columnas')->get_cantidad_filas()>0) ? $this->tabla('reporte_columnas')->get_filas() : null;
		//$columnas_a_mostrar = $this->ordenar($columnas_a_mostrar);


		//si tiene el ml de columnas me fijo el orden y cuales estan visibles
		if( isset($this->s__form_ml_columnas) ){

			$columnas_visibles = $this->s__form_ml_columnas;
			foreach ($columnas_visibles as $key => $columna) {
				if(!$columna['visible_inicial'])
					unset($columnas_visibles[$key]);
			}
			
			//busco las columnas en el orden q envio el usuario			
			foreach ($columnas_visibles as $key => $value) {

				foreach ($columnas_a_mostrar as $key2 => $value2) {
					if($value['clave'] == $value2['clave'])
						$columnas_a_mostrar_aux[] = $value2;		
				}
				
			}
			$columnas_a_mostrar = $columnas_a_mostrar_aux;
		}

		return $columnas_a_mostrar;
	}

	/* ------------------- Selector de columnas y cortes  -------------- */
	function get_cortes(){		
		return $this->tabla('reporte_cortes')->get_filas();		
	}

	function ordenar($filas){		
		uasort($filas, function($a, $b) {
			    if ($a['orden'] == $b['orden']) {
			        return 0;
			    }
			    return ($a['orden'] < $b['orden']) ? -1 : 1;
		});
		return $filas; 
	}

	// function ordenar_filtro($filtro){		
	// 	uasort($filtro, function($a, $b) {
	// 		    if ($a['orden'] == $b['orden']) {
	// 		        return 0;
	// 		    }
	// 		    return ($a['orden'] < $b['orden']) ? -1 : 1;
	// 	});
	// 	return $filtro; 
	// }
	/* ------------------------------ API ------------------------------ */
	function cargar($seleccion){
		$this->relacion()->cargar($seleccion);
		$columnas = $this->tabla('reporte_filtro')->get_filas();
		$columnas = $this->ordenar($columnas);
		$this->dep('filtro')->crear_columnas_nuevas($columnas);
	}
	function es_ordenable(){
		return $this->tabla('reporte')->get_columna('ordenable');
	}

	function extender_objeto_js(){
		echo "
			$('.ei-cuadro-fila').css('color','black');			
			$('[class^=\"ei-cuadro-cc-tit-nivel-\"]').css({
					'border':'none', 
					'list-style':'none', 
					'padding-right':'0px', 
					'padding-left':'0px'
			});
		";

		
	}
	
}
?>