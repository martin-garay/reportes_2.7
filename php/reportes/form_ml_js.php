<?php
class form_ml_js extends reportes_ei_formulario_ml
{
	//-----------------------------------------------------------------------------------
	//---- JAVASCRIPT -------------------------------------------------------------------
	//-----------------------------------------------------------------------------------

	function extender_objeto_js()
	{
		echo "
		//---- Procesamiento de EFs --------------------------------
		
		{$this->objeto_js}.evt__parametro__procesar = function(es_inicial, fila)
		{			
			if(this.ef('parametro').ir_a_fila(fila).chequeado()){
				this.ef('tipo_parametro').ir_a_fila(fila).mostrar();
			}else{
				this.ef('tipo_parametro').ir_a_fila(fila).ocultar();
			}
		
		}
		";
	}

}

?>