<?php
class ci_edicion_simple_reporte extends reportes_ci
{
	protected $s__filtro;
	//-----------------------------------------------------------------------------------
	//---- Eventos ----------------------------------------------------------------------
	//-----------------------------------------------------------------------------------

	function evt__procesar()
	{		
		try {
			$this->dep('dt_reportes')->sincronizar();
			$this->set_pantalla('pant_inicial');	
		} catch (toba_error_db $e) {
			throw new toba_error_usuario("Error al grabar el registro");			
		}	
	}

	function evt__cancelar()
	{
		$this->dep('dt_reportes')->resetear();
		$this->set_pantalla('pant_inicial');
	}

	//-----------------------------------------------------------------------------------
	//---- filtro -----------------------------------------------------------------------
	//-----------------------------------------------------------------------------------

	function conf__filtro(reportes_ei_filtro $filtro)
	{				
		if(isset($this->s__filtro))
			$filtro->set_datos($this->s__filtro);
	}

	function evt__filtro__filtrar($datos)
	{
		$this->s__filtro = $datos;
	}

	function evt__filtro__cancelar()
	{
		unset($this->s__filtro);
	}

	//-----------------------------------------------------------------------------------
	//---- cuadro -----------------------------------------------------------------------
	//-----------------------------------------------------------------------------------

	function conf__cuadro(reportes_ei_cuadro $cuadro)
	{		
		$where = (isset($this->s__filtro)) ? $this->dep('filtro')->get_sql_where() : null;
		return toba::consulta_php('parametrizacion')->get_reportes($where);
	}

	function evt__cuadro__seleccion($seleccion)
	{		
		$this->dep('dt_reportes')->cargar($seleccion);
		$this->set_pantalla('pant_edicion');
	}

	function conf_evt__cuadro__eliminar(toba_evento_usuario $evento, $fila)
	{
		$evento->anular();
	}

	function conf_evt__cuadro__visualizar(toba_evento_usuario $evento, $fila)
	{
		$evento->anular();
	}

	//-----------------------------------------------------------------------------------
	//---- form -------------------------------------------------------------------------
	//-----------------------------------------------------------------------------------

	
	function conf__form(reportes_ei_formulario $form)
	{
		return $this->dep('dt_reportes')->get();
	}

	function evt__form__modificacion($datos)
	{
		$reporte = $this->dep('dt_reportes')->get();		
		$datos['usuario_modificacion'] = toba::usuario()->get_id();
		$datos['fecha_modificacion'] = date('Y-m-d H:m:s');

		$this->dep('dt_reportes')->set(array_merge($reporte,$datos));		
	}


}
?>