CREATE TABLE public.reportes_columnas
(
  id serial NOT NULL,
  clave text NOT NULL,
  titulo text NOT NULL,
  formateo text,
  total smallint,
  total_cc text,
  grupo text,  
  no_ordenar smallint,
  permitir_html smallint,
  orden integer DEFAULT 1,  
  id_reporte integer NOT NULL,
  CONSTRAINT pk_reportes_columnas PRIMARY KEY (id),
  CONSTRAINT fk_reportes_columnas__reportes FOREIGN KEY (id_reporte)
      REFERENCES public.reportes (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);