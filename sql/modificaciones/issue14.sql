alter table reportes add column mostrar_total_registros boolean NOT NULL DEFAULT false;
alter table reportes add column ordenable boolean NOT NULL DEFAULT false;
alter table reportes add column usar_ordenamiento_con_cortes boolean NOT NULL DEFAULT false;