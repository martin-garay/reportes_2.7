# Reportes

Generador de Operaciones de Reportes para SIU-Toba

# Instalación del Proyecto

1. Cargar el entorno de toba del proyecto al que vamos a agregar el proyecto **Reportes**

```
source entorno_toba.env
```

2. Clonar el proyecto en la ubicacion deseada

```bash
git clone https://gitlab.unlu.edu.ar/mgaray/reportes-toba.git reportes
cd reportes
```

3. Generar un archivo `proyecto.ini`, `www/css/toba.css` y `php/lib/variables_reemplazo.php` valido

```bash
cp proyecto.ini.example proyecto.ini
cp php/lib/variables_reemplazo.php.example php/lib/variables_reemplazo.php
touch www/css/toba.css
```

4. Cargar el proyecto

```bash
toba proyecto cargar -p reportes -d `pwd`
```

## Configuración

* [Opcional] En ambos proyectos tildar "Listar multiproyectos" desde el Toba Editor en la pestaña "Configuración básica" de las propiedades del proyecto.

* En `proyecto.ini` descomentar la varible `proyecto_usuarios` con el identificador del proyecto del cual vamos a consultar usuarios y perfiles para dar permisos a los reportes. Por defecto es el proyecto "reportes"

```ini
[proyecto]
...
proyecto_usuarios = miproyecto
```

* Crear la base de datos del proyecto Reportes. Ejemplo:

```bash
createdb reportes_stock -U postgres
```

* Correr el script `sql/estructura/tablas.sql` para la creacion de las tablas. 

Opcionalmente usar el siguiente comando

```bash
export PATH_REPORTES=`pwd`
toba base ejecutar_sql -a $PATH_REPORTES/sql/estructura/tablas.sql -d "$TOBA_INSTANCIA reportes reportes"
```

* Configurar las fuentes de datos desde el Toba Editor. Hay 2 fuentes a configurar:
  * `reportes`: es la que guarda los metadatos de los reportes generados
  * `Negocio`: es la base sobre la que se va a consultar. (deberia ser la del proyecto base)

* A veces puede ser necesaria la ejecución del comando `cambiar_permisos`:

```bash
toba instalacion cambiar_permisos -u `whoami` -g www-data
```

(cambiar `whoami` si se quiere poner permisos sobre un usuario diferente)