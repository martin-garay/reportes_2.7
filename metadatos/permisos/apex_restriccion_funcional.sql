
------------------------------------------------------------
-- apex_restriccion_funcional
------------------------------------------------------------

--- INICIO Grupo de desarrollo 18
INSERT INTO apex_restriccion_funcional (proyecto, restriccion_funcional, descripcion, permite_edicion) VALUES (
	'reportes', --proyecto
	'18000001', --restriccion_funcional
	'ocular_sql_reporte', --descripcion
	'0'  --permite_edicion
);
--- FIN Grupo de desarrollo 18
